<?php include TEMPLATE_DIR . "header.php"; ?>

<div class="mw-layout-wrapper checkout-page">
    <div class="text-layout-section2">
        <div class="edit" field="content" rel="content">
            <h2><?php _lang('Complete your order', "templates/davy"); ?></h2>
            <div class="edit" field="checkout_page" rel="content">
                <module type="shop/checkout" id="cart_checkout"/>
            </div>
        </div>
    </div>
</div>

<?php include TEMPLATE_DIR . "footer.php"; ?>
