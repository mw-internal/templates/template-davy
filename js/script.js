$(document).ready(function(){
    mw.share();
    $(".mobile-menu").on('click', function(){
        $(document.body).toggleClass('mobile-menu-active')
    })
});

mw.share = mw.share || function(){
    $(".mw-share").each(function(){
        if(!this.mwshare){
            this.mwshare = true;
            var url = $(this).attr('data-url') || location.href;
            var sh = '<div class="mw-share-content">'
                +'<a href="https://www.facebook.com/sharer/sharer.php?u='+url+'" target="_blank"><span class="mw-icon-facebook"></span></a>'
                +'<a href="https://twitter.com/share?text=&url='+url+'" target="_blank"><span class="mw-icon-twitter"></span></a>'
                +'<a href="https://plus.google.com/share?url='+url+'" target="_blank"><span class="mw-icon-googleplus"></span></a></div>';

            $(this).append(sh)
            $(this).on('click', function(){
               $(this).toggleClass('share-active');
            });
        }
    })
}

$(document).ready(function () {
    mw.on('mw.cart.add', function (event, data) {

        if (document.getElementById('AddToCartModal') === null) {

            AddToCartModal = mw.modal({

                content: AddToCartModalContent(data.product.title),

                template: 'mw_modal_basic',

                name: "AddToCartModal",

                width: 400,

                height: 200

            });

        }

        else {

            AddToCartModal.container.innerHTML = AddToCartModalContent(data.product.title);

        }
    });
});