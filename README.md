# Davy

Davy is easy to use and clean website template based on Microweber CMS. 

It is perfect for small shops, personal blog. It contains 5 main pages and 7 custom templates layouts. Support full Blog features and full online store functionality. 

Use this template with Microweber for free to build a simple and modern website. 

## Features:

* Free Microweber Template
* 5 Main Pages (Home, Shop, About, Blog, Contacts)
* 7+ templates layouts
* 75+ Modules available
* Fully Blog functionality
* Full eCommerce Support  (Shop Page, Products List, Product Inner, Shopping Cart, and Checkout Page)
* Pixel Perfect Design
* User-Friendly Code
* Personal Design
* Fully Responsive
* Cross Browser Support
* Easy to Customize
* Well Documented
* Free Lifetime Updates
* Community Supports
