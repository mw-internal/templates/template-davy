<?php

/*

type: layout
content_type: static
name: About
position: 2
is_default: true
description: About layout

*/


?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<div class="edit" rel="content" field="davy_content">
    <module type="layouts" template="skin-4"/>
</div>

<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
