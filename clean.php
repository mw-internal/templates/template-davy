<?php

/*

type: layout
content_type: static
name: Clean
position: 2
is_default: true
description: Clean layout

*/


?>

<?php include THIS_TEMPLATE_DIR . "header.php"; ?>
<div class="edit" rel="content" field="davy_content">
    <module type="layouts" template="skin-3"/>
</div>
<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
