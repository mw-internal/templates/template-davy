            </div>

            <footer id="footer">
                <div class="mw-ui-row">
                    <div class="mw-ui-col">
                        <div><br/>
                            <?php print powered_by_link(); ?>
                        </div>
                    </div>
                    <div class="mw-ui-col">
                        <module type="social_links" id="footer-social" show-icons="facebook,twitter,googleplus,pinterest,youtube">
                    </div>
                </div>
            </footer>

        </div>
    </body>
</html>