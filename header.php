<!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#">
<head>
    <title>{content_meta_title}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta property="og:title" content="{content_meta_title}"/>
    <meta name="keywords" content="{content_meta_keywords}"/>
    <meta name="description" content="{content_meta_description}"/>
    <meta property="og:type" content="{og_type}"/>
    <meta property="og:url" content="{content_url}"/>
    <meta property="og:image" content="{content_image}"/>
    <meta property="og:description" content="{og_description}"/>
    <meta property="og:site_name" content="{og_site_name}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="alternate" type="application/rss+xml" title="{og_site_name}" href="<?php print site_url('rss'); ?>"/>

    <link href="<?php print template_url(); ?>css/style.css" rel="stylesheet" type="text/css"/>

    <script src="<?php print template_url(); ?>js/script.js"></script>

    <script>
        AddToCartModalContent = window.AddToCartModalContent || function (title) {
                var html = ''

                    + '<section class="text-center" style="text-align: center;">'

                    + '<span class="sm-icon-bag2"></span>'

                    + '<h4>' + title + '</h4>'

                    + '<p><br /><?php _lang("has been added to your cart", "templates/davy"); ?><br /><br /></p>'

                    + '<a href="javascript:;" onclick="mw.tools.modal.remove(\'#AddToCartModal\')" class="mw-ui-btn m-10"><?php _lang("Continue shopping", "templates/davy"); ?></a> &nbsp; '

                    + '<a href="<?php print checkout_url(); ?>" class="mw-ui-btn mw-ui-btn-invert m-10"><?php _lang("Checkout", "templates/davy"); ?></a></section>';

                return html;
            }
    </script>
</head>
<body>
<div id="container">
    <?php $shopping_cart = get_option('shopping-cart', 'mw-template-davy'); ?>
    <?php if ($shopping_cart == 'true'): ?>
    <module type="shop/cart" template="davy_header" id="header-cart">
        <?php endif; ?>
        <header id="header" class="edit" field="davy_header" rel="global">
            <div class="mw-ui-row">
                <div class="mw-ui-col">
                    <div>51 avenue, 7 street</div>
                </div>
                <div class="mw-ui-col">
                    <module type="logo" template="default" id="header-logo" text="DAVI" font_family="Algerian" font_size="70">
                </div>
                <div class="mw-ui-col">
                    <div><p style="text-align: right">(800) 123-4567</p></div>
                </div>
            </div>

            <nav>
                <span class="mobile-menu"><span></span><span></span><span></span></span>
                <module type="menu" template="davy" id="header-menu">
            </nav>
        </header>
        <div id="content">