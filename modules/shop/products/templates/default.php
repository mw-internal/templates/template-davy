<?php

/*

type: layout

name: Smarty

description: smarty

*/
?>



<?php


$tn = $tn_size;
if(!isset($tn[0]) or ($tn[0]) == 150){
     $tn[0] = 300;
}
if(!isset($tn[1])){
     $tn[1] = $tn[0];
}

?>
<?php  $rand = uniqid(); ?>
<script>
    if(!window.Packery){
        mw.require('<?php print  template_url() ?>modules/shop/products/templates/packery.pkgd.min.js');
    }
</script>

<script>
    $(document).ready(function(){
        $("#posts-<?php print $rand; ?> ul").packery({
            gutter:".shop-item-listgutter",
            itemSelector: '.shop-item-list-item',
            percentPosition: true
        })
    })
</script>



<div class="clearfix module-products-template-tplsmarty <?php print count($data) < 3 ? 'module-products-template-tplsmarty-center' : ''; ?>" id="posts-<?php print $rand; ?>">
  <?php if (!empty($data)): ?>



    <ul class="shop-item-list">

    <?php



    $count = -1;
    foreach ($data as $item):

    $count++;

    ?>


    <li class="shop-item-list-item">

        <div class="shop-item clearfix">

            <div class="thumbnail">
                <!-- product image(s) -->
                    <?php if($show_fields == false or in_array('thumbnail', $show_fields)): ?>
                        <a class="shop-item-image" href="<?php print $item['link'] ?>">
                            <?php
                                $images = get_pictures($item['id']);


                            ?>
                            <?php if(isset($images[0]['filename'])){  ?>
                                
                                <span style="background-image: url(<?php print $images[0]['filename'] ?>)"></span>
                            <?php   } ?>

                            <?php if(isset($images[1]['filename'])){  ?>
                                <span style="background-image: url(<?php print $images[1]['filename'] ?>)"></span>
                            <?php   } ?>

                        </a>
                    <?php endif; ?>
                <!-- /product image(s) -->
            </div>

            <div class="shop-item-summary">
                <div class="shop-price-area">
                     <?php if($show_fields == false or in_array('price', $show_fields)): ?>
        <?php if(isset($item['prices']) and is_array($item['prices'])){  ?>
	    <?php
		$vals2 = array_values($item['prices']);
		$val1 = array_shift($vals2); ?>

             <span class="shop-item-price">
                   <?php print currency_format($val1); ?>
                </span>
                &nbsp;
         <?php } else{ ?>

        <?php } ?>
        <?php endif; ?>
                <?php if($show_fields == false or in_array('title', $show_fields)): ?>
                    <a href="<?php print $item['link'] ?>"><?php print $item['title'] ?></a>
                <?php endif; ?>
               </div>
                  <?php if($show_fields == false or ($show_fields != false and  is_array($show_fields) and  in_array('description', $show_fields))): ?>
                <p><!-- product short description -->
                    <?php print $item['description']; ?>
                </p><!-- /product short description -->

                 <?php endif; ?>

                 <?php if($show_fields != false and ($show_fields != false and  in_array('read_more', $show_fields))): ?>

        <a href="<?php print $item['link'] ?>" class="mw-more"><?php $read_more_text ? print $read_more_text : print _e('Read More', true); ?></a>

      <?php endif; ?>


                <!-- price -->

                <!-- /price -->

                <?php if($show_fields == false or in_array('add_to_cart', $show_fields)): ?>
        <?php
                $add_cart_text = get_option('data-add-to-cart-text', $params['id']);
              if( $add_cart_text == false){  $add_cart_text =  _e("Add to cart", true); }

        ?>

                 <?php if(is_array( $item['prices'])): ?>
                 <div class="shop-item-buttons">
                <a class="btn btn-default" onclick="mw.cart.add('.mw-add-to-cart-<?php print $item['id'].$count ?>');"><i class="icon-shopping-cart glyphicon glyphicon-shopping-cart"></i>&nbsp;<?php print $add_cart_text ?></a>
                </div><?php endif; ?>
        <?php endif; ?>

              


               <?php if(is_array( $item['prices']) ): ?>
      <?php foreach( $item['prices']  as $k  => $v): ?>

      <div class="clear products-list-proceholder mw-add-to-cart-<?php print $item['id'].$count ?>">
        <input type="hidden"  name="price" value="<?php print $v ?>" />
        <input type="hidden"  name="content_id" value="<?php print $item['id'] ?>" />
      </div>
      <?php break; endforeach ; ?>
      <?php endif; ?>


            </div>

        </div>

    </li>


    <?php  endforeach; ?>
    <li class="shop-item-listgutter"></li>
    </ul>
  <?php endif; ?>
</div>
<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
<?php print paging("num={$pages_count}&paging_param={$paging_param}") ?>
<?php endif; ?>
