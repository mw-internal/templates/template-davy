<?php

/*

type: layout

name: Add to cart default

description: Add to cart default

*/
?>
<?php

if (isset($params['content-id'])) {
    $product = get_content_by_id($params["content-id"]);
    $title = $product['title'];
} else {
    $title = _e("Product", true);
}


?>
<script>mw.moduleCSS("<?php print modules_url(); ?>shop/cart_add/templates.css")</script>

<br class="mw-add-to-cart-spacer"/>
<module type="custom_fields" data-content-id="<?php print intval($for_id); ?>" data-skip-type="price" id="cart_fields_<?php print $params['id'] ?>"/>

<?php if (is_array($data)): ?>
    <div class="price">
        <div class="clearfix"></div>
        <table class="pull-right">
            <?php $i = 1;
            foreach ($data as $key => $v): ?>
                <tr>
                    <td>
                        <div class="mw-price-item" style="text-align: right; font-size: 25px; padding: 15px 0; padding-right: 25px;">
                            <span class="mw-price">
                              <strong>
                                  <?php if (is_string($key) and trim(strtolower($key)) == 'price'): ?>
                                      <?php #_e($key); ?>
                                  <?php else: ?>
                                      <?php print $key; ?>: &nbsp;
                                  <?php endif; ?>
                            </strong>

                                <span class="price"><?php print mw()->shop_manager->currency_format($v); ?></span>
                            </span>

                        </div>
                    </td>
                    <td>
                        <?php if (!isset($in_stock) or $in_stock == false) : ?>
                            <button class="btn btn-primary pull-right product-add-cart noradius" type="button" disabled="disabled"
                                    onclick="Alert('<?php print addslashes(_("This item is out of stock and cannot be ordered", true)); ?>');"><i
                                        class="icon-shopping-cart glyphicon glyphicon-shopping-cart"></i>
                                <?php _lang("Out of stock", "templates/davy"); ?>
                            </button>
                        <?php else: ?>
                            <button class="btn btn-primary pull-right product-add-cart noradius" type="button"
                                    onclick="mw.cart.add('.mw-add-to-cart-<?php print $params['id'] ?>','<?php print $v ?>', '<?php print $title; ?>');">
                                <?php _lang("Add to cart", "templates/davy"); ?>
                            </button>
                            <?php $i++; endif; ?>
                    </td>

                    <?php if ($i > 1) : ?>
                        <br/>
                    <?php endif; ?>
                </tr>
                <?php $i++; endforeach; ?>
        </table>
        <div class="clearfix"></div>
    </div>

<?php endif; ?>
<div class="clearfix"></div>
<div class="row">
    <span class="mw-share"><span class="mw-icon-web-share"></span> Share Product</span>
</div>

<div class="clearfix"></div>
