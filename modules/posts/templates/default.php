<?php

/*

type: layout

name: Default (Davy)

description: Davy

*/
?>
<?php  $rand = uniqid(); ?>


<?php




 ?>

<div class="clearfix module-posts-template-davy" id="posts-<?php print $rand; ?>">
  <?php if (!empty($data)): ?>

  <?php
        $count = -1;
        foreach ($data as $item):
        $count++;
    ?>
  <div class="module-posts-template-davy-item">



    <?php if(!isset($show_fields) or $show_fields == false or in_array('thumbnail', $show_fields)): ?>
        <a href="<?php print $item['link'] ?>" class="module-posts-template-davy-item-image">
            <img src="<?php print $item['image']; ?>" alt="" class="full-width" />
        </a>
    <?php endif; ?>
   <div class="davy-post-info">
    <?php if(!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
        <h2 class="page-title">
                <a href="<?php print $item['link'] ?>"><?php print $item['title'] ?></a>
               <?php if(!isset($show_fields) or $show_fields == false or in_array('created_at', $show_fields)): ?>
                <small class="date"> <?php print ($item['created_at']); ?></small>
               <?php endif; ?>            
        </h2>
  <?php endif; ?>

    <div class="module-posts-template-davy-item-container">

      <?php if(!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)): ?>
      <p class="description"><?php print $item['description'] ?></p>
      <?php endif; ?>
      <?php if(!isset($show_fields) or $show_fields == false or in_array('read_more', $show_fields)): ?>
      <div class="blog-post-footer"> <a href="<?php print $item['link'] ?>" class="btn btn-default pull-fleft">
        <?php $read_more_text ? print $read_more_text : print _e('Continue Reading', true); ?>
        <i class="icon-chevron-right"></i></a> </div>
      <?php endif; ?>
    </div>
    </div>
    <div class="share-bar"><div><span class="mw-share" data-url="<?php print $item['link']; ?>"><span class="mw-icon-web-share"></span> Share this</span></div></div>
  </div>
  <?php endforeach; ?>
  <?php endif; ?>
</div>
<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
<?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
