<?php

/*

type: layout

name: Head banner with title

position: 3

*/

?>

<div class="nodrop safe-mode edit" field="layout-skin-4-<?php print $params['id'] ?>" rel="module">
    <div class="richtext">
        <div class="title-img">
            <img src="<?php print template_url(); ?>img/about1.jpg" alt=""/>
            <h2 class="title">About this website</h2>
        </div>
        <div></div>
    </div>
</div>