<?php

/*

type: layout

name: Contact form

position: 5

*/

?>

<div class="nodrop safe-mode edit" field="layout-skin-6-<?php print $params['id'] ?>" rel="module">
    <div class="richtext">
        <div class="mw-ui-row">
            <div class="mw-ui-col" style="width: 50%">
                <div class="mw-ui-col-container" id="contacttextcol">
                    <div class="richtext allow-drop">
                        <h2 class="page-title" style="text-align: left">Contact Us</h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                            printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                        <div class="mw-row">
                            <div class="mw-col">
                                Phone: 00359 878 95878<br>
                                E-mail: info@yourdomain.com

                            </div>
                            <div class="mw-col">
                                Website: yourdomain.com<br>
                                Address: CA, 5505, 7 avenue , flore 4<br>
                                Find us Map

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mw-ui-col" style="width: 50%">
                <div class="mw-ui-col-container contact-form">
                    <module type="contact_form">
                </div>
            </div>
        </div>
    </div>
</div>