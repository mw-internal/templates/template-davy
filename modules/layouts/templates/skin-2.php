<?php

/*

type: layout

name: Image with text

position: 1

*/

?>

<div class="nodrop safe-mode edit" field="layout-skin-2-<?php print $params['id'] ?>" rel="module">
    <div class="richtext home allow-drop">
        <img src="<?php print template_url(); ?>img/home.jpg" class="full-width">
        <br><br>
        <h2 class="title" style="text-align: center;">About this website</h2>
        <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
            galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It
            was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions
            of Lorem Ipsum.
        </p>
        <br>
        <p style="text-align: center"><a href="<?php print site_url(); ?>blog" class="more-link">View Artists <span class="mw-icon-app-arrow-thin-right"></span></a></p>
    </div>
</div>