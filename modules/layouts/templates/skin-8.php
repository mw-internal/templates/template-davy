<?php

/*

type: layout

name: Blog

position: 7

*/
?>

<div class="nodrop safe-mode edit" field="layout-skin-8-<?php print $params['id'] ?>" rel="module">
    <div class="blog-page">
        <module content-id="<?php print PAGE_ID; ?>" type="posts" data-description-length="1150" data-show="thumbnail,title,description,created_at"/>
    </div>
</div>