<?php

/*

type: layout

name: Shop

position: 6

*/
?>

<div class="nodrop safe-mode edit" field="layout-skin-7-<?php print $params['id'] ?>" rel="module">
    <section class="container">
        <div class="mw-layout-wrapper">
            <h2 class="page-title">Online Shop</h2>
            <module type="shop/products" limit="18" description-length="70" data-show="title,thumbnail,price" hide-paging="n"/>
        </div>
    </section>
</div>