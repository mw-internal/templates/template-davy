<?php

/*

  type: layout

  name: Default

  description: Davy template for Contact form
  



*/

?>


<div class="contact-form-container contact-form-template-davy">
    <div class="contact-form">

        <form class="mw_form" data-form-id="<?php print $form_id ?>" name="<?php print $form_id ?>" method="post">


            <module type="custom_fields" data-id="<?php print $params['id'] ?>" data-for="module" default-fields="name,email,message"/>

            <script>

                $(document).ready(function () {
                    var fields = $(".contact-form-template-davy .module-custom-fields .control-group");
                    if (fields.length > 0) {
                        var last = fields[fields.length - 1];
                        fields.each(function (i) {
                            var index = i + 1;
                            if (index === fields.length && index % 2 !== 0) {
                                $(this).addClass('full-width');
                            }
                            else {
                                $(this).addClass((index % 2 !== 0 ? 'odd' : 'even'));
                            }
                        });
                    }

                })

            </script>

            <?php if (get_option('disable_captcha', $params['id']) != 'y'): ?>
                <div class="mw-ui-row-nodrop captcha-holder">
                    <module type="captcha"/>
                </div>
                <button type="submit" class="mw-template-btn mw-template-btn-invert"><?php _lang("Send", "templates/davy"); ?></button>
            <?php else: ?>
                <button type="submit" class="mw-template-btn mw-template-btn-invert"><?php _lang("Send", "templates/davy"); ?></button>
            <?php endif; ?>
        </form>
    </div>
    <div class="clearfix"></div>

    <div class="message-sent" id="msg<?php print $form_id ?>">
        <span class="message-sent-icon"></span>
        <p><?php _lang("Your Email was sent successfully", "templates/davy"); ?></p>
    </div>
</div>
