<?php

/*

type: layout

name: Davy

description: Davy Menu skin

*/

?>



  <?php
      $menu_filter = array();

      $menu_filter['ul_class'] = '';

      $menu_filter['ul_class_deep'] = '';

      $menu_filter = menu_tree($menu_filter);

      if($menu_filter != false){
        print ($menu_filter);
      }
      else{
        print lnotif("There are no items in the menu <b>".$params['menu-name']. '</b>');
      }
  ?>
