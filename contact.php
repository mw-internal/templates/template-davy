<?php

/*

type: layout
content_type: static
name: Contacts
position: 2
is_default: true
description: Contacts layout

*/


?>
<?php include THIS_TEMPLATE_DIR. "header.php"; ?>

<div class="edit" rel="content" field="davy_content">
    <module type="layouts" template="skin-4"/>
    <module type="layouts" template="skin-6"/>
</div>

<?php include THIS_TEMPLATE_DIR. "footer.php"; ?>
